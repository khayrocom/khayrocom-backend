var student=require('../models/student');
var institute=require('../models/institute');
var classe=require('../models/class');
var mongoose=require('mongoose');
var express=require('express');
var app = express();
var bodyParser=require('body-parser');

exports.Getallinstitute=function(req,res){
    institute.find({},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.GetinstituteBy_id=function(req,res){
    institute.findOne({_id:req.params.id},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.GetinstituteBy_iduser=function(req,res){
    institute.find({'idusers':req.params.iduser},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.GetinstituteBy_InstituteName=function(req,res){
    institute.find({'InstituteName':req.params.InstituteName},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.Addinstitute=function(req,res){
    delete req.body.token;

    var new_publicatio=new institute(req.body);
    new_publicatio.save(function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"institute added"};
        console.log(response);
        res.json(response);
    })
}
var ObjectId = require('mongoose').Types.ObjectId;

exports.Updateinstitute=function(req,res){
	    var data =req.body;//.toObject()
    delete data._id;
    institute.update({'_id':ObjectId(req.query.id)}, {$set:data}, function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"institute updated"};
        console.log(response);
        res.json(response);
    })
}


exports.instdeleteuser=function(req,res){
	    var data =req.body;//.toObject()
   // delete data._id;
    institute.update({}, { $pull: { idusers:data.iduser } },{ multi: true }, function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"institute updated"};
        console.log(response);
        res.json(response);
    })
}

exports.Updateinstitutelistuser=function(req,res){
	    var data =req.body;//.toObject()
    delete data._id;
    institute.update({'_id':ObjectId(req.query.id)}, {$addToSet:data}, function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"institute updated"};
        console.log(response);
        res.json(response);
    })
}
exports.Deleteinstitute=function(req, res){
    institute.deleteOne({'_id':req.query.idinstitute}, function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"institute deleted"};
        console.log(response);
        res.json(response);
    })
}



exports.Getinstitutedetail=function(req, res) {
	institute.findById(req.query.id, function (err, institute) {
		if (err) return res.status(500).send("There was a problem finding the institute.");
		if (!institute) return res.status(404).send("No institute found.");
		res.status(200).send(institute);
	});
// add the middleware function
	// app.use(function (user, req, res, next) {
	// 	res.status(200).send(user);
	// });

};

//var searchable = require('mongoose-searchable');

exports.search= function(req, res){
//     institute.search(req.body.keyword, function(error, foundinstitute) {
//     	if (error) return res.status(500).json({error: "Internal Server Error"});
//     	res.status(200).json(foundinstitute);    
// });
var keyword=req.query.q;
var query = { 

	$or: [
        { InstituteName: {  $regex: new RegExp(keyword), $options: 'i' } },
        { Halaqaname: {  $regex: new RegExp(keyword), $options: 'i' } },
        { City: {  $regex: new RegExp(keyword), $options: 'i' } }
    ]};//, $language: 'en'


    institute.find(query,{'-_id':0,'__v':0},function(err, posts) {
        if (err) return res.status(500).json({error: "Internal Server Error"});
        res.status(200).json(posts);
    }).limit(10);
}

exports.searchlocal= function(req, res){
//     institute.search(req.body.keyword, function(error, foundinstitute) {
//     	if (error) return res.status(500).json({error: "Internal Server Error"});
//     	res.status(200).json(foundinstitute);    
// });
var keyword=req.query.q;
var query = { $and:[
	{iduser:req.query.iduser},
	{$or: [
        { InstituteName: {  $regex: new RegExp(keyword), $options: 'i' } },
        { Halaqaname: {  $regex: new RegExp(keyword), $options: 'i' } },
        { City: {  $regex: new RegExp(keyword), $options: 'i' } }
    ]}]};//, $language: 'en'


    institute.find(query,{'-_id':0,'__v':0},function(err, posts) {
        if (err) return res.status(500).json({error: "Internal Server Error"});
        res.status(200).json(posts);
    }).limit(10);
}

/*exports.DeleteSensoAll=function(req, res){
    sensor.deleted( function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"sensor deleted"};
        console.log(response);
        res.json(response);
    })
}*/


  function listToMatrix(list, elementsPerSubArray) {
     var matrix = [], i, k;
     for (i = 0, k = -1; i < list.length; i++) {
         if (i % elementsPerSubArray === 0) {
             k++;
             matrix[k] = [];
         }
         matrix[k].push(list[i]);
     }
     console.log('i lit to matrix',i);
    let r=i%elementsPerSubArray;
    if(r!=0){
     for (let l = 0; l < elementsPerSubArray-r; l++) {
         matrix[k].push('----');
     }}
     return matrix;
 }
// exports.report= function(req, res){
// //     institute.search(req.body.keyword, function(error, foundinstitute) {
// //      if (error) return res.status(500).json({error: "Internal Server Error"});
// //      res.status(200).json(foundinstitute);    
// // });
// var response=[]
// var async = require("async");
// var idinstitute=req.query.idinstitute;
// var FromDate=req.body.FromDate;
// var ToDate=req.body.ToDate;
// var Notes=req.body.Notes;
//  var content1=[];
// var ListStudentNames=[];
// // var query = { $and:[
// //   {FromDate:req.body.FromDate},
// //   {ToDate:req.body.ToDate},
// //   {Notes:req.body.Notes},
// //   { $or: [
// //           { InstituteName: {  $regex: new RegExp(keyword), $options: 'i' } },

// //         { StudentName: {  $regex: new RegExp(keyword), $options: 'i' } },
// //         { ClassName: {  $regex: new RegExp(keyword), $options: 'i' } }
// //     ]}

// //     ]};

//   console.log('1111');
// var classres=[];
//   async.series([ 
//         function(callback) {

//       classe.find({'idinstitute':idinstitute},function(err, classes) {
//         if (err) return res.status(500).json({error: "Internal Server Error"});
//           console.log('2222');
// //classres=classes;
//    callback(null, classes);
//     })
//   }],

// function(err, resclass) {
// if(err){console.log(err)}
  



//   else{
//   async.series([ 
// //   	 function(callback) {
// // //.limit(10);
// // //    callback(null, response);

// //     },
//         function(callback) {
// for(let classresult of resclass){
// 	  console.log('3333');
//     student.find({'idclass':classresult._id},function(err, students) {
//         if (err) return res.status(500).json({error: "Internal Server Error"});
// callback(null, students);
//                     })
// }
// }

//  ],
// function(err, resstudents) {
// if(err){console.log(err)}
//   else{

//   	for(let studentsresult of resstudents){
// 		ListStudentNames.push(studentsresult.StudentName);
//     achievement.find({'idstudent':studentsresult.idstudent},function(err,achievements){
//         if (err) return res.status(500).json({error: "Internal Server Error"});
// })
// 	}


// }
//     // results is now equal to ['one', 'two']
// });

// }
//     // results is now equal to ['one', 'two']
// });


	
//   content1.push({
//               style: 'itemsTable',
//               table: {
//   //  widths: ['auto', 'auto','auto', 'auto','auto',  'auto', 'auto'],
//   //  headerRows: 2,
//     // keepWithHeaderRows: 1,
//     body: [
//       //[{text: 'Header with Colspan = 2', style: 'tableHeader', colSpan: 2, alignment: 'center'}, {}, {text: 'Header 3', style: 'tableHeader', alignment: 'center'}],
//       //[{text: 'Header 1', style: 'tableHeader', alignment: 'center'}, {text: 'Header 2', style: 'tableHeader', alignment: 'center'}, {text: 'Header 3', style: 'tableHeader', alignment: 'center'}],
//       [{rowSpan:2,fillColor: '#cccccc',text:'Dates'},{fillColor: '#cccccc',text:'from'},
//       {colSpan:3,text:FromDate},'','', {fillColor: '#cccccc',rowSpan:2,text:'Month'},{rowSpan:2,text:'Dates'}],

//       ['',{fillColor: '#cccccc',text:'to'}, {colSpan:3,text:ToDate},'','', '',''],
//       [{rowSpan:2,fillColor: '#cccccc',text:'Memorize and improvement'},{rowSpan:2,fillColor: '#cccccc',text:'Memorized'},
//        {text:'pages'},{text:'Surahs'},{text:'verses'}, {fillColor: '#cccccc',text:'Students motivation'},{  text:'Satisfactory'}],

//        ['','',{text:'50'},{text:'30'},{text:'500'}, {fillColor: '#cccccc',text:'Students level'},{text:'good'}],
//       [{colSpan:2,fillColor: '#cccccc',text:'nember of student'},'', {colSpan:3,text:ListStudentNames.length},'','',
//       {fillColor: '#cccccc',text:'class'},{text:'ClassName'}],
//       [{colSpan:2,fillColor: '#cccccc',text: 'Student names'},'',{colSpan: 5, table: {body:[].concat(listToMatrix(ListStudentNames,3))}},'','','',''],
//       [{colSpan:2,fillColor: '#cccccc',text:'Notes'},'',{colSpan:5,text:Notes},'','','','']
//     ]
//   }
// });
// }
//     callback(null, content1);

// }

//  ],
// function(err, results) {
// if(err){console.log(err)}
//   else{
//   res.status(200).json(results);
//  // console.log(response);
//   console.log('resulta',results)

// }
//     // results is now equal to ['one', 'two']
// });

// }
//     // results is now equal to ['one', 'two']
// });






 
// }