var express=require('express');
var app=express();
var bodyParser=require('body-parser');
var VerifyToken=require('./VerifyToken');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended:true,
}));
//var passport	= require('passport');


module.exports=function(app){
	var user_ctrl=require('./userroutes');
	var institute_ctrl=require('./instituteroutes')
	var halaqa_ctrl=require('./halaqaroutes')
	var class_ctrl=require('./classroutes')
	var student_ctrl=require('./studentroutes')
	var achievement_ctrl=require('./achievementroutes')

	//users controller
	app.post('/login',user_ctrl.Getuser);
	app.get('/allusers/:token',VerifyToken,user_ctrl.Getalluser);
	app.get('/userinfo',VerifyToken,user_ctrl.Getuserdetail);
	//app.get('/infomember',passport.authenticate('jwt', { session: false}),user_ctrl.getinfo);
	app.get('/gettoken',user_ctrl.gettoken);


	//app.get('/verifytoken/:token',VerifyToken);
	//app.post('/verifytoken',VerifyToken);
	app.get('/verifytoken/:token',VerifyToken,user_ctrl.getinfo);

	app.post('/users',user_ctrl.adduser);
	app.put('/users/:id',user_ctrl.UpdateUser);
	app.delete('/users',VerifyToken,user_ctrl.DeleteUser);
	app.post('/resetpw',user_ctrl.resetpassword);
	app.post('/midresetpw',user_ctrl.resetpasswordfinaltache);
	app.post('/endresetpw',user_ctrl.pwdchange);
	//app.post('/search',user_ctrl.search);



//institute controller
	app.get('/institute/:token',VerifyToken,institute_ctrl.Getallinstitute);
	app.get('/institute/:id/:token',VerifyToken,institute_ctrl.GetinstituteBy_id);
	app.get('/institutebyuserid/:iduser/:token',VerifyToken,institute_ctrl.GetinstituteBy_iduser);
	app.get('/institutebyInstituteName/:InstituteName',VerifyToken,institute_ctrl.GetinstituteBy_InstituteName);
	app.post('/institute',VerifyToken,institute_ctrl.Addinstitute);
	app.put('/instupdate',VerifyToken,institute_ctrl.Updateinstitute);
	app.put('/instdeleteuser',VerifyToken,institute_ctrl.instdeleteuser);
	app.put('/Upinstlistuser',VerifyToken,institute_ctrl.Updateinstitutelistuser);
	app.delete('/institute',VerifyToken,institute_ctrl.Deleteinstitute);
	app.get('/institutesearch',institute_ctrl.search);
	app.get('/institutesearchbyuser',institute_ctrl.searchlocal);
	app.get('/instinfo',VerifyToken,institute_ctrl.Getinstitutedetail);
	//app.put('/report',institute_ctrl.report);


//halaqa controller
	app.get('/halaqa/:token',VerifyToken,halaqa_ctrl.Gethalaqa);
	app.get('/halaqainstitute/:InstituteName/:token',VerifyToken,halaqa_ctrl.Gethalaqaunderinstitute);
	app.get('/halaqabyinstituteid/:idinstitute/:token',VerifyToken,halaqa_ctrl.GethalaqaBy_idinstitute);
	app.post('/halaqa',VerifyToken,halaqa_ctrl.Addhalaqa);
	app.put('/halaqaupdate',VerifyToken,halaqa_ctrl.Updatehalaqa);
	app.delete('/halaqa/:id',VerifyToken,halaqa_ctrl.Deletehalaqa);
	app.get('/halaqasearch',halaqa_ctrl.search);
	app.get('/halaqasearchbyinstitute',halaqa_ctrl.searchlocal);
	app.get('/halaqainfo',VerifyToken,halaqa_ctrl.Gethalaqadetail);

//class controller
	app.get('/class/:token',VerifyToken,class_ctrl.Getclasse);
	app.get('/classinstitute/:InstituteName/:token',VerifyToken,class_ctrl.Getclasseunderinstitute);
	app.get('/classbyinstituteid/:idinstitute/:token',VerifyToken,class_ctrl.GetclasseBy_idinstitute);
	app.post('/class',VerifyToken,class_ctrl.Addclasse);
	app.put('/classupdate',VerifyToken,class_ctrl.Updateclasse);
	app.delete('/class/:id',VerifyToken,class_ctrl.Deleteclasse);
	app.get('/classsearch',class_ctrl.search);
	app.get('/classsearchbyinstitute',class_ctrl.searchlocal);
	app.get('/classinfo',VerifyToken,class_ctrl.Getclassdetail);
//student controller
	app.get('/student/:token',VerifyToken,student_ctrl.Getstudent);
	app.get('/studentbyclassid/:idclass/:token',student_ctrl.GetstudentBy_idclass);
	app.get('/studentbyhalaqaid/:idhalaqa/:token',student_ctrl.GetstudentBy_idhalaqa);
	app.get('/studentdoublefilter/:idhalaqa/:idclass/:token',student_ctrl.studentdoublefilter);
	app.post('/student',VerifyToken,student_ctrl.Addstudent);
	app.put('/stupdate',VerifyToken,student_ctrl.Updatestudent);
	app.delete('/student/:id',VerifyToken,student_ctrl.Deletestudent);
	app.get('/studentsearch',student_ctrl.search);
	app.get('/studentsearchbyhalaqa',student_ctrl.searchlocalbyhalaqa);	
	app.get('/studentsearchbyhalaqaandclass',student_ctrl.searchlocalbyclassandhalaqa);
	app.get('/studentinfo',VerifyToken,student_ctrl.Getstudentdetail);

//achievement controller
	app.get('/achievement/:token',VerifyToken,achievement_ctrl.Getachievement);
	app.get('/achievementbystudentid/:idstudent/:token',VerifyToken,achievement_ctrl.GetachievementBy_idstudent);
	app.post('/achievement',VerifyToken,achievement_ctrl.Addachievement);
	app.put('/achievement/:id',VerifyToken,achievement_ctrl.Updateachievement);
	app.delete('/achievement/:id',VerifyToken,achievement_ctrl.Deleteachievement);





//global search

	app.get('/globsearch',user_ctrl.search);


}
