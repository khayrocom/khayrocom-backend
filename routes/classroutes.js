var classe=require('../models/class');
var mongoose=require('mongoose');
var express=require('express');
var app=express();
var bodyParser=require('body-parser');

exports.Getclasse=function(req,res){
    classe.find({},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.Getclasseunderinstitute=function(req,res){
    classe.find({'InstituteName':req.params.InstituteName},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.GetclasseBy_id=function(req,res){
    classe.find({'_id':req.params.id},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.GetclasseBy_idinstitute=function(req,res){
    classe.find({'idinstitute':req.params.idinstitute},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.Getclassdetail=function(req, res) {
    classe.findById(req.query.id, function (err, classe) {
        if (err) return res.status(500).send("There was a problem finding the class.");
        if (!classe) return res.status(404).send("No class found.");
        res.status(200).send(classe);
    });
// add the middleware function
    // app.use(function (user, req, res, next) {
    //  res.status(200).send(user);
    // });

};
exports.Addclasse=function(req,res){
        delete req.body.token;

    var new_publicatio=new classe(req.body);
    new_publicatio.save(function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"classe added"};
        console.log(response);
        res.json(response);
    })
}
var ObjectId = require('mongoose').Types.ObjectId;

exports.Updateclasse=function(req,res){
       var data =req.body;//.toObject()
    delete data._id;
    classe.update({'_id':ObjectId(req.query.id)}, {$set:data}, function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"classe updated"};
        console.log(response);
        res.json(response);
    })
}
exports.Deleteclasse=function(req, res){
    classe.deleteOne({'_id':req.params.id}, function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"classe deleted"};
        console.log(response);
        res.json(response);
    })
}

exports.search= function(req, res){
//     institute.search(req.body.keyword, function(error, foundinstitute) {
//      if (error) return res.status(500).json({error: "Internal Server Error"});
//      res.status(200).json(foundinstitute);    
// });
var keyword=req.query.q;
var query = { $or: [
    
        { ClassName: {  $regex: new RegExp(keyword), $options: 'i' } }
    ]};//, $language: 'en'


    classe.find(query,{'-_id':0,'__v':0},function(err, posts) {
        if (err) return res.status(500).json({error: "Internal Server Error"});
        res.status(200).json(posts);
    }).limit(10);
}

exports.searchlocal= function(req, res){
//     institute.search(req.body.keyword, function(error, foundinstitute) {
//      if (error) return res.status(500).json({error: "Internal Server Error"});
//      res.status(200).json(foundinstitute);    
// });
var keyword=req.query.q;
var query = { $and:[
    {idinstitute:req.query.idinstitute},
    {$or: [
{ ClassName: {  $regex: new RegExp(keyword), $options: 'i' } }
    ]}]};//, $language: 'en'


    classe.find(query,{'-_id':0,'__v':0},function(err, posts) {
        if (err) return res.status(500).json({error: "Internal Server Error"});
        res.status(200).json(posts);
    }).limit(10);
}
/*exports.DeleteSensoAll=function(req, res){
    sensor.deleted( function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"sensor deleted"};
        console.log(response);
        res.json(response);
    })
}*/