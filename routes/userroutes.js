var express = require('express');
var mongoose=require('mongoose');
var app=express();
var crypto = require('crypto');
// var Reservation=require('../models/Reservation_meeting');
var user=require('../models/User')
var student=require('../models/student');
var institute=require('../models/institute');
var halaqa=require('../models/halaqa');
var classe=require('../models/class');
var bodyParser=require('body-parser');
//var jwt = require('jwt-simple');
var jwt = require('jsonwebtoken');
var mailer = require('nodemailer');

/*app.use(bodyParser.urlencoded({
	extended:true
}));*/
/*var passport	= require('passport');


require('../mytxt/passport')(passport);*/

var genRandomString = function(length){
	return crypto.randomBytes(Math.ceil(length/2))
		.toString('hex') /** convert to hexadecimal format */
		.slice(0,length);   /** return required number of characters */
};

var sha512 = function(password, salt){
	var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
	hash.update(password);
	var value = hash.digest('hex');
	return {
		salt:salt,
		password:value
	};
};

function saltHashPassword(userpassword) {
	var salt = genRandomString(16); /** Gives us salt of length 16 */
	var passwordData = sha512(userpassword, salt);
	console.log('UserPassword = '+userpassword);
	console.log('Passwordhash = '+passwordData.password);
	console.log('nSalt = '+passwordData.salt);
}

exports.adduser= function (req, res) {
			//return res.json({ "success": false, "msg": "Error while creating user", "error": err });

	var salt = genRandomString(16); /** Gives us salt of length 16 */
	var passwordData = sha512(req.body.password, salt);
	console.log('UserPassword = '+req.body.password);
	console.log('Passwordhash = '+passwordData.password);
	console.log('nSalt = '+passwordData.salt);
			//return res.json({ "success": false, "msg": "Error while creating user", "error": err });

	var newuser = new user({
		username: req.body.username,
		password: passwordData.password,
		email: req.body.email,
		phone: req.body.phone,
		type: req.body.type,

		salt: passwordData.salt,
	});
	newuser.save(function (err) {
		if (err) {
			console.log("some error: ", err);
			return res.json({ "success": "false", "msg": "Error while creating user", "error": err });
		}

			var token = jwt.sign({ id: newuser._id }, "-----BEGIN RSA PRIVATE KEY-----\n"+config.privateKey+"\n-----END RSA PRIVATE KEY-----", {
			expiresIn: 86400, // expires in 24 hours
			algorithm: "RS256"
		});
		//res.status(200).send({ auth: true, token: token });

		res.status(201).send({ "success": "true", "msg": 'Successful created new user.', "result": newuser,token: token });
	})
}






exports.UpdateUser=function(req,res){
	user.update({'_id':req.params.id}, req.body, function(err, data){
		if(err){
            var response=[];
            console.log(err);
		}
		else
			var response={"message":"user updated"};
        console.log(response);
		res.json(response);
	})
}
exports.DeleteUser=function(req, res){
	user.deleteOne({'_id':req.query.iduser}, function(err, data){
		if(err){
            var response=[];
            console.log(err);
		}
		else
			var response={"message":"user deleted"};
        console.log(response);
		res.json(response);
	})
}

exports.Getuserdetail=function(req,res){
    user.find({'_id':req.params.id},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
       var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.Getalluser=function(req,res){
    user.find({},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
//  npm install node-cache --save
/////////////////////////////////////////////////////////////////////////////reset
var randomstring = require("randomstring");

const NodeCache = require( "node-cache" );
global.myCache = new NodeCache();
exports.resetpassword =function(req, res) {
  //var token = getToken(req.headers);
    //var decoded = jwt.decode(token, config.publicKey);
    user.findOne({email:req.body.email}, function(err, user) {
if (err) {
            return res.status(401).send({auth: false, token: 'null ok'});
		}
else{	//var response=JSON.parse(user);
//npm install uuid
     // Save the user's email and a password reset hash in session. We will use
//npm install randomstring
var passwordResetHash = randomstring.generate(17);
var salt = genRandomString(16); /** Gives us salt of length 16 */
	var passwordData = sha512(passwordResetHash, salt).password;
        //me.session.passwordResetHash = passwordResetHash;
        //me.session.emailWhoRequestedPasswordReset = user.email;
        //const myCache = new NodeCache();
    var passreset={passwordResetHash:passwordData,salt:salt}
myCache.set( "mykey", passreset, 300000, function( err, success ){
  if( !err && success ){
    console.log( success );
    // true
    // ... do something ...
  }
});

var transporter = mailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'khayrocom.app@gmail.com',
    pass: 'Khayrocom2019**'
  }
});
  // setup email data with unicode symbols
var mailOptions = {
  from: 'khayrocom.app@gmail.com',
  to: user.email,
  subject: 'Sending Email using Node.js',
  text:  'Dear '+user.username+',\n\nSomeone (hopefully you) has requested a password reset for your khayrocom account.\n\nTo do so, you can enter the following password reset code:\n\n\t\t'
	+ passwordResetHash +'\n\nIf you have not made this change, ignore this email. Please do not reply to this automatic email.\n\nCordially,\nKhayrocom TEAM'};
//console.log("salt",)};
//console.log("salt",)
  // send mail with defined transport object
  let info =  transporter.sendMail(mailOptions)

  //      mailer.sendPasswordResetHash(user.email, passwordResetHash);
        //return callback(err, new me.ApiResponse({ success: true, extras: { passwordResetHash: passwordResetHash } }));
res.json(user);


   } }
);

};

exports.resetpasswordfinaltache =function(req, res) {
var recivedcode=req.body.emailcode;

myCache.get( "mykey", function( err, value ){

var verifcode=  sha512(recivedcode, value.salt).password;
if(verifcode===value.passwordResetHash){
	 response1={verification:"true"};
}
else { response1={verification:"false"};}
})
console.log("verification="+response1);
res.json(response1);
}

exports.pwdchange= function (req, res) {
			//return res.json({ "success": false, "msg": "Error while creating user", "error": err });

	var salt = genRandomString(16); /** Gives us salt of length 16 */
	var passwordData = sha512(req.body.password, salt).password;
	console.log('UserPassword = '+req.body.password);
	console.log('Passwordhash = '+passwordData.password);
	console.log('nSalt = '+passwordData.salt);
			//return res.json({ "success": false, "msg": "Error while creating user", "error": err });

var uppw={password: passwordData,salt:salt}

	user.update({email:req.body.email},uppw, function(err, data){
		if(err){
            var response=[];
            console.log(err);
		}
		else
			var response={"message":"user updated"};
        console.log(response);
		res.json(response);
	})
}
///////////////////////////////////////////////////////////////////////////////////////////////




exports.Getuser= function (req, res) {


	user.findOne({email: req.body.email}, function(err, user){

		if (err) {
            return res.status(401).send({auth: false, token: 'null ok'});
		}
     if (user) {
         var salt = user.salt;
         var passwordData = sha512(String(req.body.password), salt).password;
         if (passwordData === user.password) {
             var token = jwt.sign({id: user._id}, "-----BEGIN RSA PRIVATE KEY-----\n" + config.privateKey + "\n-----END RSA PRIVATE KEY-----", {
                 expiresIn: 86400, // expires in 24 hours
                 algorithm: "RS256"
             });
             res.status(200).send({auth: true, token: token,iduser:user._id,role:user.role});
         } else {
             res.status(401).send({auth: false, token: 'null oui'});
         }

     }
     else
	 {return res.status(401).send({auth: false, token: 'null 1'});}
	});

};

exports.gettoken= function(req, res) {
console.log('req.userId', req.query.iduser);
	user.findById(req.query.iduser, function (err, user) {
		if (err) return res.status(500).send("There was a problem finding the user.");
		if (!user) return res.status(404).send("No user found.");
		res.status(200).send(user);
	});
// add the middleware function
	// app.use(function (user, req, res, next) {
	// 	res.status(200).send(user);
	// });

};



exports.getinfo  =function(req, res,next) {
  //var token = getToken(req.headers);

    var decoded = jwt.decode(req.params.token, config.publicKey);
    user.findById(req.userId, function(err, user) {
        if (err) throw err;

        if (!user) {
          return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
        } else {
          res.json({success: true, msg: 'Welcome in the member area ' + user.username + '!',iduser:user._id,email:user.email,username:user.username,role:user.role});
        }
    });

};




exports.search= function(req, res){
//     institute.search(req.body.keyword, function(error, foundinstitute) {
//      if (error) return res.status(500).json({error: "Internal Server Error"});
//      res.status(200).json(foundinstitute);    
// });
var response=[]
var async = require("async");
var keyword=req.query.q;
var query = { $and:[
  { $or: [{iduser:req.query.iduser},{'idusers':req.query.iduser}]},
  { $or: [
          { InstituteName: {  $regex: new RegExp(keyword), $options: 'i' } },
          { HalaqaName: {  $regex: new RegExp(keyword), $options: 'i' } },

        { StudentName: {  $regex: new RegExp(keyword), $options: 'i' } },
        { ClassName: {  $regex: new RegExp(keyword), $options: 'i' } }
    ]}

    ]};
  async.series([ 
        function(callback) {
    student.find(query,{'-_id':0,'__v':0},function(err, posts) {
        if (err) return res.status(500).json({error: "Internal Server Error"});
    callback(null, posts);
    }).limit(10);
}
,
    function(callback) {


     institute.find(query,{'-_id':0,'__v':0},function(err, posts) {
        if (err) return res.status(500).json({error: "Internal Server Error"});
    callback(null, posts);
    }).limit(10);

},
    function(callback) {

      classe.find(query,{'-_id':0,'__v':0},function(err, posts) {
        if (err) return res.status(500).json({error: "Internal Server Error"});
    callback(null, posts);
    }).limit(10);
//    callback(null, response);

    },
       function(callback) {

      halaqa.find(query,{'-_id':0,'__v':0},function(err, posts) {
        if (err) return res.status(500).json({error: "Internal Server Error"});
    callback(null, posts);
    }).limit(10);
//    callback(null, response);

    }
 ],
function(err, results) {
if(err){console.log(err)}
  else{
  res.status(200).json(results);
 // console.log(response);
  console.log('resulta',results)

}
    // results is now equal to ['one', 'two']
});
}

/*verfyToken=function(req, res, next) {

    // check header or url parameters or post parameters for token
    var token = req.headers['x-access-token'];
    if (!token)
        return res.status(403).send({ auth: false, message: 'No token provided.' });

    // verifies secret and checks exp
    jwt.verify(token, "-----BEGIN PUBLIC KEY-----\n"+config.publicKey+"\n-----END PUBLIC KEY-----", {algorithm: "RS256"},function(err, decoded) {
        if (err)
            return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });

        // if everything is good, save to request for use in other routes
        req.userId = decoded.id;
        next();
    });

}

*/
//module.exports= app;
/*getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};*/
