var halaqa=require('../models/halaqa');
var mongoose=require('mongoose');
var express=require('express');
var app=express();
var bodyParser=require('body-parser');

exports.Gethalaqa=function(req,res){
    halaqa.find({},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.Gethalaqaunderinstitute=function(req,res){
    halaqa.find({'InstituteName':req.params.InstituteName},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.GethalaqaBy_id=function(req,res){
    halaqa.find({'_id':req.params.id},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.GethalaqaBy_idinstitute=function(req,res){
    halaqa.find({'idinstitute':req.params.idinstitute},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.Gethalaqadetail=function(req, res) {
    halaqa.findById(req.query.id, function (err, halaqa) {
        if (err) return res.status(500).send("There was a problem finding the halaqa.");
        if (!halaqa) return res.status(404).send("No halaqa found.");
        res.status(200).send(halaqa);
    });
// add the middleware function
    // app.use(function (user, req, res, next) {
    //  res.status(200).send(user);
    // });

};
exports.Addhalaqa=function(req,res){
        delete req.body.token;

    var new_publicatio=new halaqa(req.body);
    new_publicatio.save(function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"halaqa added"};
        console.log(response);
        res.json(response);
    })
}
var ObjectId = require('mongoose').Types.ObjectId;

exports.Updatehalaqa=function(req,res){
       var data =req.body;//.toObject()
    delete data._id;
    halaqa.update({'_id':ObjectId(req.query.id)}, {$set:data}, function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"halaqa updated"};
        console.log(response);
        res.json(response);
    })
}
exports.Deletehalaqa=function(req, res){
    halaqa.deleteOne({'_id':req.params.id}, function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"halaqa deleted"};
        console.log(response);
        res.json(response);
    })
}

exports.search= function(req, res){
//     institute.search(req.body.keyword, function(error, foundinstitute) {
//      if (error) return res.status(500).json({error: "Internal Server Error"});
//      res.status(200).json(foundinstitute);    
// });
var keyword=req.query.q;
var query = { $or: [
    
        { HalaqaName: {  $regex: new RegExp(keyword), $options: 'i' } }
    ]};//, $language: 'en'


    halaqa.find(query,{'-_id':0,'__v':0},function(err, posts) {
        if (err) return res.status(500).json({error: "Internal Server Error"});
        res.status(200).json(posts);
    }).limit(10);
}

exports.searchlocal= function(req, res){
//     institute.search(req.body.keyword, function(error, foundinstitute) {
//      if (error) return res.status(500).json({error: "Internal Server Error"});
//      res.status(200).json(foundinstitute);    
// });
var keyword=req.query.q;
var query = { $and:[
    {idinstitute:req.query.idinstitute},
    {$or: [
{ HalaqaName: {  $regex: new RegExp(keyword), $options: 'i' } }
    ]}]};//, $language: 'en'


    halaqa.find(query,{'-_id':0,'__v':0},function(err, posts) {
        if (err) return res.status(500).json({error: "Internal Server Error"});
        res.status(200).json(posts);
    }).limit(10);
}
/*exports.DeleteSensoAll=function(req, res){
    sensor.deleted( function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"sensor deleted"};
        console.log(response);
        res.json(response);
    })
}*/