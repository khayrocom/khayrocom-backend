var achievement=require('../models/achievement');
var mongoose=require('mongoose');
var express=require('express');
var app=express();
var bodyParser=require('body-parser');

exports.Getachievement=function(req,res){
    achievement.find({},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.GetachievementBy_id=function(req,res){
    achievement.find({'_id':req.params.id},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.GetachievementBy_idstudent=function(req,res){
    achievement.find({'idstudent':req.params.idstudent},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.Addachievement=function(req,res){
        delete req.body.token;

    var new_publicatio=new achievement(req.body);
    new_publicatio.save(function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"achievement added"};
        console.log(response);
        res.json(response);
    })
}
exports.Updateachievement=function(req,res){
    achievement.update({'_id':req.params.id}, req.body, function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"achievement updated"};
        console.log(response);
        res.json(response);
    })
}
exports.Deleteachievement=function(req, res){
    achievement.deleteOne({'_id':req.params.id}, function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"achievement deleted"};
        console.log(response);
        res.json(response);
    })
}
/*exports.DeleteSensoAll=function(req, res){
    sensor.deleted( function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"sensor deleted"};
        console.log(response);
        res.json(response);
    })
}*/