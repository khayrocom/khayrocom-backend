var student=require('../models/student');
var mongoose=require('mongoose');
var express=require('express');
var app=express();
var bodyParser=require('body-parser');

exports.Getstudent=function(req,res){
    student.find({},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.GetstudentBy_id=function(req,res){
    student.find({'_id':req.params.id},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.studentdoublefilter=function(req,res){
    student.find( { $and:[{'idclass':req.params.idclass,'idhalaqa':req.params.idhalaqa}]},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.GetstudentBy_idclass=function(req,res){
    student.find({'idclass':req.params.idclass},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.GetstudentBy_idhalaqa=function(req,res){
    student.find({'idhalaqa':req.params.idhalaqa},function(err,data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response=data;
        console.log(response);
        res.json(response);
    })
}
exports.Addstudent=function(req,res){
        delete req.body.token;

    var new_publicatio=new student(req.body);
    new_publicatio.save(function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"student added"};
        console.log(response);
        res.json(response);
    })
}
var ObjectId = require('mongoose').Types.ObjectId;

exports.Updatestudent=function(req,res){
    var data =req.body;//.toObject()
    delete data._id;

    student.updateOne({"_id":ObjectId(req.query.id)}, {$set:data}, function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"student updated"};
        console.log(response);
        res.json(response);
    })
}
exports.Deletestudent=function(req, res){
    student.deleteOne({'_id':req.params.id}, function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"student deleted"};
        console.log(response);
        res.json(response);
    })
}
exports.Getstudentdetail=function(req, res) {
    student.findById(req.query.id, function (err, student) {
        if (err) return res.status(500).json({error:"There was a problem finding the student."});
        if (!student) return res.status(404).json({error:"No student found."});
        res.status(200).json(student);
    });
// add the middleware function
    // app.use(function (user, req, res, next) {
    //  res.status(200).send(user);
    // });

};
exports.search= function(req, res){
//     institute.search(req.body.keyword, function(error, foundinstitute) {
//      if (error) return res.status(500).json({error: "Internal Server Error"});
//      res.status(200).json(foundinstitute);    
// });
var keyword=req.query.q;
var query = { $or: [
        { StudentName: {  $regex: new RegExp(keyword), $options: 'i' } },
        { RoomNumber: {  $regex: new RegExp(keyword), $options: 'i' } },
        { Status: {  $regex: new RegExp(keyword), $options: 'i' } },
        { ExitDate: {  $regex: new RegExp(keyword), $options: 'i' } },
        { EntryDate: {  $regex: new RegExp(keyword), $options: 'i' } }
    ]};//, $language: 'en'


    student.find(query,{'-_id':0,'__v':0},function(err, posts) {
        if (err) return res.status(500).json({error: "Internal Server Error"});
        res.status(200).json(posts);
    }).limit(10);
}
exports.searchlocalbyhalaqa= function(req, res){
//     institute.search(req.body.keyword, function(error, foundinstitute) {
//      if (error) return res.status(500).json({error: "Internal Server Error"});
//      res.status(200).json(foundinstitute);    
// });
var keyword=req.query.q;
var query = { $and:[
    {idhalaqa:req.query.idhalaqa},
    {$or: [
         { StudentName: {  $regex: new RegExp(keyword), $options: 'i' } },
        { RoomNumber: {  $regex: new RegExp(keyword), $options: 'i' } },
        { Status: {  $regex: new RegExp(keyword), $options: 'i' } },
        { ExitDate: {  $regex: new RegExp(keyword), $options: 'i' } },
        { EntryDate: {  $regex: new RegExp(keyword), $options: 'i' } }
    ]}

    ]};//, $language: 'en'


    student.find(query,{'-_id':0,'__v':0},function(err, posts) {
        if (err) return res.status(500).json({error: "Internal Server Error"});
        res.status(200).json(posts);
    }).limit(10);
}
exports.searchlocalbyclassandhalaqa= function(req, res){
//     institute.search(req.body.keyword, function(error, foundinstitute) {
//      if (error) return res.status(500).json({error: "Internal Server Error"});
//      res.status(200).json(foundinstitute);    
// });
var keyword=req.query.q;
var query = { $and:[
    { $and:[{idhalaqa:req.query.idhalaqa},{idclass:req.query.idclass}]},
    {$or: [
         { StudentName: {  $regex: new RegExp(keyword), $options: 'i' } },
        { RoomNumber: {  $regex: new RegExp(keyword), $options: 'i' } },
        { Status: {  $regex: new RegExp(keyword), $options: 'i' } },
        { ExitDate: {  $regex: new RegExp(keyword), $options: 'i' } },
        { EntryDate: {  $regex: new RegExp(keyword), $options: 'i' } }
    ]}

    ]};//, $language: 'en'


    student.find(query,{'-_id':0,'__v':0},function(err, posts) {
        if (err) return res.status(500).json({error: "Internal Server Error"});
        res.status(200).json(posts);
    }).limit(10);
}
/*exports.DeleteSensoAll=function(req, res){
    sensor.deleted( function(err, data){
        if(err){
            var response=[];
            console.log(err);
        }
        else
            var response={"message":"sensor deleted"};
        console.log(response);
        res.json(response);
    })
}*/