/**
 * Created by Asus on 02/12/2017.
 */
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('../config'); // get our config file

function verifyToken(req, res, next) {

    // check header or url parameters or post parameters for token
    //var token = req.body.token;//req.params.token;
    //res=[];
    if (req.body.token)
        var token = req.body.token;
    else if(req.params.token)
        var token = req.params.token;
    else
        var token = req.query.token;
    if (!token)
        return res.status(403).send({ auth: false, message: 'No token provided ok.' });

    // verifies secret and checks exp
    jwt.verify(token, "-----BEGIN PUBLIC KEY-----\n"+config.publicKey+"\n-----END PUBLIC KEY-----", {algorithm: "RS256"},function(err, decoded) {
        if (err)
            return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });

        // if everything is good, save to request for use in other routes
        req.userId = decoded.id;
        next();
    });

}

module.exports = verifyToken;