var mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator')
require('mongoose-type-email');

var UserSchema = new mongoose.Schema({

    username: {
      type: String,
       required: true,
       //unique: true
     },

    password: {
      type: String,
      required: true
    },
    email: {
      type: mongoose.SchemaTypes.Email,
       required: true,
       unique: true
     },
    phone: {
      type: String,
      required: false
    },
    role: {
      type: String,
      default: 'user'
    },
    salt: {
      type: String,
       required: true
     },
    created_at: Date
});
    UserSchema.pre('save', function (next) {
        var user = this;
        // get the current date
        var currentDate = new Date();

        // if created_at doesn't exist, add to that field
        if (!user.created_at) {
            user.created_at = currentDate;
        }
        next();

} );
//UserSchema.plugin(uniqueValidator)
UserSchema.index({email:1},{unique:true});
module.exports = mongoose.model('User', UserSchema);
