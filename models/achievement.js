var mongoose=require('mongoose');

var achievementSchema=new mongoose.Schema({
	 /*Temperature: {
      type: Number,
       //unique: true
     },
    
    positionlag: {
      type: Number,
    },
    positionlat: {
      type: Number,
     },
    himidité: {
      type: Number,
    },
    type: {
      type: String,
      required: true
    },
   created_at: Date*/
  idstudent: {type: String, required: true},

   'AchievDate':{ type:String, required: true,default: Date()},
			'Program': { type:String, required: true},
      Surah: {
      type: String,
       required: true,
       //unique: true
     },
      'FromVerse': { type:String, required: true},
      'ToVerse': { type:String, required: true},
      'Evaluation': { type:String, required: true},
      'Notes': { type:String, required: true}
});
    achievementSchema.pre('save', function (next) {
        var achievement = this;
        // get the current date
        var currentDate = new Date();

        // if created_at doesn't exist, add to that field
        if (!achievement.created_at) {
            achievement.created_at = currentDate;
        }
        next();

} );
module.exports=mongoose.model('achievement',achievementSchema);