var mongoose=require('mongoose');

var studentSchema=new mongoose.Schema({
	 /*Temperature: {
      type: Number,
       //unique: true
     },
    
    positionlag: {
      type: Number,
    },
    positionlat: {
      type: Number,
     },
    himidité: {
      type: Number,
    },
    type: {
      type: String,
      required: true
    },
   created_at: Date*/
    idhalaqa: {type: String, required: true},
    idclass: {type: String, required: true},
    iduser: {type: String, required: true},
    StudentName: {
      type: String,
       required: true,
       //unique: true
     },
			'Gender': String,
      Status:  {type: String, required: true},
      archive:  {type: String, default:'false'},
      'RoomNumber': String,
      'Level': String,
      'EntryDate': String,
      'ExitDate': String
});
    studentSchema.pre('save', function (next) {
        var student = this;
        // get the current date
        var currentDate = new Date();

        // if created_at doesn't exist, add to that field
        if (!student.created_at) {
            student.created_at = currentDate;
        }
        next();

} );
        studentSchema.index({
    StudentName: 'text',
    RoomNumber: 'text',
    Status: 'text',
    ExitDate: 'text',
    EntryDate: 'text'
});
module.exports=mongoose.model('student',studentSchema);