var mongoose=require('mongoose');
var searchable = require('mongoose-searchable');

var instituteSchema=new mongoose.Schema({
	 /*   created_at: Date*/
    iduser: {type: String, required: true},
idusers : { type : Array , "default" : [] },


   InstituteName: {
      type: String,
       required: true,
       //unique: true
     },
			'Country': String,
			'City': String,
		//	'Halaqaname': String
});
    instituteSchema.pre('save', function (next) {
        var institute = this;
        // get the current date
        var currentDate = new Date();

        // if created_at doesn't exist, add to that field
        if (!institute.created_at) {
            institute.created_at = currentDate;
        }
        next();

} );

//     instituteSchema.plugin(searchable,{
//     keywordField:'keywords',
//     language:'english',
//     fields:['InstituteName','Halaqaname','City'],
//     // //blacklist:['comic','batman'],
//     extract: function(content, done){
//         done(null, content.split(' '));//
//     }
// });

     instituteSchema.index({
    InstituteName: 'text',
    Halaqaname: 'text',
    City: 'text'
});
module.exports=mongoose.model('institute',instituteSchema);