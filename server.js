var express =require ('express');
var path= require('path');
cors = require('cors');
helmet = require('helmet');
var bodyParser =require ('body-parser');
var index = require ('./routes/Routes');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var passport	= require('passport');
//var jwt = require('jwt-simple');
var jwt = require('jsonwebtoken');


//const mqtt = require('mqtt')


var app= express();
var port =process.env.PORT ||3005;
var errorhandler = require('errorhandler');
config=require ('./config.json');

var mongoose = require('mongoose');
mongoose.connect(config.database, function(err) {
  if(err) {
    console.log('connection error', err);
  } else {
    console.log('connection successful');
  }
});
mongoose.set('useCreateIndex', true);





/*var client=mqtt.connect('mqtt://localhost:1883');

client.on('connect',function () {
	client.subscribe('Temperature');
	client.subscribe('positionlag');
	client.subscribe('positionlat');
	client.subscribe('hymidité');
	setInterval(function () {
		var x=Math.random();
		client.publish('positionlat',(parseFloat(x*17+3).toFixed(2)).toString());
		client.publish('Temperature',(parseInt(x*10+200)).toString());
		client.publish('positionlag',(parseFloat(x*17+3).toFixed(2)).toString());
		client.publish('hymidité',(parseInt(x*10+200)).toString());

    },10000)
});
client.on('message',function (topic,message){
	 var Temperature  = "0";
  var positionlat = "0";
  var positionlag = "0";
   var hymidité  = "0";
  
  if(topic.toString() == "Temperature"){
  		Temperature = message.toString();
  		console.log('Temperature '+Temperature );
  }
  if(topic.toString() == "positionlag"){
  		positionlag = message.toString();
  		console.log('positionlag '+positionlag );
  }
  if(topic.toString() == "positionlat"){
  		positionlat = message.toString();
  		console.log('positionlat '+positionlat );
  }
  if(topic.toString() == "hymidité"){
  		hymidité = message.toString();
  		console.log('hymidité  '+hymidité );
  }
  
  var newstatus = sensorModel({
			Temperature: Temperature,
			positionlat: positionlat,
			positionlag: positionlag,
			hymidité: hymidité
		});
 	newstatus.save(function (err) {
		if (err) {
			console.log("some error: ", err);
			return res.json({ "success": false, "msg": "Error while creating user", "error": err });
		}
		console.log("seccess");
	});

	//console.log(topic.toString()+": "+message.toString());
})

*/
app.use(bodyParser.json());
app.use(cors());	
app.use(helmet())

app.use(passport.initialize());
/*app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});*/
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
require('./routes/Routes')(app);

//app.use("/",index);
if (process.env.NODE_ENV === 'development') {
  app.use(logger('dev'));
  app.use(errorhandler())
}

app.listen(port,function()
{
	console.log('server strated on port' + port);
});
